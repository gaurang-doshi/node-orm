var Status = require("../Config/Status");
var Constant = require("../Config/Constant");

var TokenGenerator = require("../Config/TokenGenerator");
var tokenGenerator = new TokenGenerator();

var UserSchema = require("../Schema/Users");
var UserTokenSchema = require("../Schema/UserToken");

var bluebird = require("bluebird");
var Parallel = bluebird.join;

class UsersModel {
  constructor() {
    console.log("UsersModel");
    this.userSchema = UserSchema;
    this.userTokensSchema = UserTokenSchema;
  }

  login(res, data) {
    return new Promise((resolve, reject) => {
      this.userSchema
        .findOne({
          where: {
            email: data.email,
          },
        })
        .then((user) => {
          if (user) {
            let Encrypt = require("./../Config/Encrypt");
            let encrypt = new Encrypt();

            let isValid = encrypt.compareEntity(
              data.password,
              user.dataValues.password
            );
            if (isValid) {
              delete user.dataValues["password"];
              delete user.dataValues["created_at"];
              delete user.dataValues["updated_at"];

              let token = tokenGenerator.generator();

              let usersTokenData = {
                user_id: user.id,
                app_token: token,
                device_type: data.deviceInfo.device_type,
                device_token: data.deviceInfo.device_token,
                build_version: data.deviceInfo.build_version,
              };

              user.dataValues["app_token"] = token;
              this.userTokensSchema
                .create(usersTokenData)

                .then((userToken) => {
                  resolve({
                    status: Status.CODES.SUCCESS.CODE,
                    message: Status.CODES.SUCCESS.MESSAGE,
                    data: user,
                  });
                })
                .catch((error) => {
                  reject({
                    status: Status.CODES.SERVER_ERROR.CODE,
                    message: Status.CODES.SERVER_ERROR.MESSAGE,
                    error: error,
                  });
                });
            } else {
              resolve({
                status: Status.CODES.CONFLICT.CODE,
                message: res.Status.MESSAGES.PASSWORD_INCORRECT,
              });
            }
          }
        });
    });
  }

  add(res, data) {
    return new Promise((resolve, reject) => {
      //console.log('data',data.email);
      let token = tokenGenerator.generator();
      this.userSchema
        .findAll({
          where: {
            [database.Op.or]: [
              {
                email: data.email,
              },
            ],
          },
        })
        .then((users) => {
          if (users.length > 0) {
            users.forEach((user) => {
              if (user.dataValues.email === data.email) {
                resolve({
                  status: Status.CODES.CONFLICT.CODE,
                  message: Status.MESSAGES.EMAIL_EXISTS,
                });
              }
            });
          } else {
            data.user_tokens = {
              app_token: token,
              device_type: data.deviceInfo.device_type,
              device_token: data.deviceInfo.device_token,
              build_version: data.deviceInfo.build_version,
            };

            this.userSchema
              .create(data, {
                include: [UserTokenSchema],
              })
              .then((user) => {
                //console.log(user)
                // DELETING UNNECESSARY FIELDS
                delete user.dataValues["password"];
                //delete user.dataValues['created_at'];
                delete user.dataValues["updated_at"];
                delete user.dataValues["user_tokens"];

                user.dataValues["app_token"] = token;
                resolve({
                  status: Status.CODES.SUCCESS.CODE,
                  message: Status.CODES.SUCCESS.MESSAGE,
                  data: user.dataValues,
                });
              })
              .catch((error) => {
                reject({
                  status: Status.CODES.SERVER_ERROR.CODE,
                  message: Status.CODES.SERVER_ERROR.MESSAGE,
                  error: error,
                });
              });
          }
        })
        .catch((error) => {
          reject({
            status: Status.CODES.SERVER_ERROR.CODE,
            message: Status.CODES.SERVER_ERROR.MESSAGE,
            error: error,
          });
        });
    });
  }

  changePassword(res, data, header) {
    return new Promise((resolve, reject) => {
      Parallel(this.getUserByID(header.user_id), (userById) => {
        let Encrypt = require("../Config/Encrypt");
        let encrypt = new Encrypt();

        let isValid = encrypt.compareEntity(
          data.password,
          userById[0].dataValues.password
        );
        if (isValid) {
          resolve({
            status: Status.CODES.CONFLICT.CODE,
            message: Status.MESSAGES.PASSWORD_INCORRECT,
          });
        } else {
          this.userSchema
            .update({
              where: {
                user_id: data.user_id,
              },
            })
            .spread((result) => {
              resolve({
                status: Status.CODES.SUCCESS.CODE,
                message: Status.MESSAGES.PASSWORD_CHANGE,
              });
            })
            .catch((error) => {
              reject({
                status: Status.CODES.SERVER_ERROR.CODE,
                message: Status.CODES.SERVER_ERROR.MESSAGE,
                error: error,
              });
            });
        }
      }).catch((error) => {
        reject(error);
      });
    });
  }

  getUserByID(user_id) {
    return new Promise((resolve, reject) => {
      this.userSchema
        .findAll({
          where: {
            id: user_id,
          },
          include: [userTokensSchema],
        })
        .then((user) => {
          resolve(user);
        })
        .catch((error) => {
          reject({
            Status: Status.CODES.SERVER_ERROR.CODE,
          });
        });
    });
  }
}

module.exports = UsersModel;
