var Status = require("./../../Config/Status");

var UsersModel = require("../../Models/UsersModel");
var usersModel = new UsersModel();

class UsersController {
  constructor() {
    console.log("UsersController");
  }

  login(req, res) {
    usersModel
      .login(res, req.body)
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  }

  register(req, res) {
    if (req.body.isError) {
      req.body.error_details.message = req.body.error_details.message;
      req.send(req.body.error_details);
    }

    usersModel
      .add(res, req.body)
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log("Error", error);
        res.send(error);
      });
  }

  changePassword(req, res) {
    usersModel
      .changePassword(res, req.body, req.headers)
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log("Error ", error);
        res.send(error);
      });
  }
}

module.exports = UsersController;
