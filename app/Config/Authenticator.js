const Status = require("./Status");
let UserSchema = require("../Schema/Users");
let UserToken = require("../Schema/UserToken");

class Authenticator {
  authenticateUser(req, res, next) {
    let user_id = req.headers.user_id;
    let app_token = req.headers.app_token;

    if (!user_id) {
      res.send({
        status: Status.CODES.MISSING_AUTHENTICATION_DATA.CODE,
        message: "User id is not passed",
      });
    } else if (!app_token) {
      res.send({
        status: Status.CODES.MISSING_AUTHENTICATION_DATA.CODE,
        message: "App token is not passed",
      });
    } else {
      UserToken.findOne({
        where: {
          user_id: user_id,
          app_token: app_token,
        },
        attribute: ["user_id"],
        include: [
          {
            model: UserSchema,
          },
        ],
      })
        .then((user) => {
          if (user) {
            next();
          } else {
            res.send({
              status: Status.CODES.UNAUTHORIZED.CODE,
              message: Status.CODES.UNAUTHORIZED.MESSAGE,
            });
            return;
          }
        })
        .catch((error) => {
          res.send({
            status: Status.CODES.SERVER_ERROR.CODE,
            message: Status.CODES.SERVER_ERROR.CODE,
          });
          return;
        });
    }
  }
}

module.exports = Authenticator;
