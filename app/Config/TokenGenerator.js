var randomstring = require("randomstring");
var bcrypt = require("bcrypt-nodejs");
var saltRounds = 10;

class TokenGenerator {
  generator() {
    var tokenString = randomstring.generate(25);
    var token = bcrypt.hashSync(tokenString, bcrypt.genSaltSync(10), null);
    return token;
  }
}

module.exports = TokenGenerator;
