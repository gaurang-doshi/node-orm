exports.CODES = {
  CONFLICT: {
    CODE: 409,
    MESSAGE: "Provided information already exist!",
  },
  FORBIDDEN: {
    CODE: 403,
    MESSAGE: "Forbidden Error !",
  },
  MISSING_AUTHENTICATION_DATA: {
    CODE: 417,
    MESSAGE: "Authentication Information Missing",
  },
  NOT_ALLOWED: {
    CODE: 405,
    MESSAGE: "Method not allowed !",
  },
  NOT_FOUND: {
    CODE: 404,
    MESSAGE: "Requested resource not found !",
  },
  PRE_CONDITION: {
    CODE: 412,
    MESSAGE: "Please complete other steps first",
  },
  SERVER_ERROR: {
    CODE: 500,
    MESSAGE: "Internal Server Error",
  },
  SUCCESS: {
    CODE: 200,
    MESSAGE: "Success",
  },
  UNAUTHORIZED: {
    CODE: 401,
    MESSAGE: "You are not authorized to access this location.",
  },
};

exports.MESSAGES = {
  ADDRESS_UPDATED: "Your address Updated",

  ACCOUNT_NOT_FOUND: "Sorry, we can’t find your account. Please try again.",
  ACCOUNT_REGISTERED_AS_SEEKER: "This account is registered as Seeker",
  ACCOUNT_REGISTERED_AS_EMPLOYER: "This account is registered as Employer",
  EMAIL_EXISTS: "Email already exist!",
  EMAIL_IS_REGISTERED: "Email already registered!",
  EMAIL_IS_NOT_REGISTERED: "Email is not registered!",

  INTRO_VIDEO_DELETED: "Successfully deleted !",
  MEDIA_DELETED: "Successfully deleted !",

  PASSWORD_INCORRECT: "Incorrect Password !",
  PROFILE_UPDATED: "Profile details updated",
  RESET_PASSWORD: "We have sent email to your account",

  SOCIAL_ACCOUNT_EXISTS:
    "Provided social account is already linked with other account!",
  SOCIAL_ACCOUNT_NOT_REGISTERED: "Social account is not registered !",

  USER_EXISTS: "User already exist!",
  USER_STATUS_CHANGE: "User status changed successfully!",
};
