exports.DATABASE = {
  HOST: "127.0.0.1",
  NAME: "node_orm",
  USER: "root",
  PASSWORD: "",
};

exports.EMAIL = {
  SERVICE: "gmail",
  HOST: "smtp.gmail.com",
  PORT: 465,
  USERNAME: "",
  PASSWORD: "",
};
