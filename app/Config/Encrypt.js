var bcrypt = require("bcrypt-nodejs");
var saltRounds = 10;

class Encrypt {
  encryptEntity(entity) {
    return bcrypt.hashSync(entity, bcrypt.genSaltSync(saltRounds), null);
  }

  compareEntity(entity, encryptEntity) {
    return bcrypt.compareSync(entity, encryptEntity);
  }
}

module.exports = Encrypt;
