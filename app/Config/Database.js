var credential = require("./Credential");

var connection = new Sequelize(
  credential.DATABASE.NAME,
  credential.DATABASE.USER,
  credential.DATABASE.PASSWORD,
  {
    host: credential.DATABASE.HOST,
    dialect: "mysql",
    logging: false,
    operatorsAliases: Sequelize.Op,
    define: {
      underscored: true,
    },
  }
);

connection
  .authenticate()
  .then(() => {
    console.log("Connected to Database  :)");
  })
  .catch((err) => {
    console.log("Failed to connect to Database  :( \n", err);
  });

module.exports = {
  database: connection,
};
