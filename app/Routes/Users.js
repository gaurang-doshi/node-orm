var express = require("express");
var router = express.Router();

var UserController = require("../Controllers/API/UsersController");
var UserController = new UserController();

var Authenticator = require("../Config/Authenticator");
var Authenticator = new Authenticator();

router.route("/register").post(UserController.register);

router.route("/login").get(UserController.login);

router
  .route("/change-password")
  .post(Authenticator.authenticateUser, UserController.changePassword);

module.exports = router;
