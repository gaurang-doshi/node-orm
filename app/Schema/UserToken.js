var user_tokens = database.define("user_tokens", {
  app_token: {
    type: Sequelize.STRING(100),
    defaultValue: "",
  },
  build_version: {
    type: Sequelize.STRING(10),
    defaultValue: "",
  },
  device_type: {
    type: Sequelize.INTEGER(1),
    defaultValue: 0,
  },
  device_token: {
    type: Sequelize.STRING(255),
    defaultValue: "",
  },
  is_web: {
    type: Sequelize.BOOLEAN,
    defaultValue: 0,
  },
});

var users = require("./Users");
user_tokens.belongsTo(users, {
  foreignKey: "user_id",
  onDelete: "cascade",
});

users.hasMany(user_tokens, {
  foreignKey: "user_id",
  onDelete: "cascade",
});

user_tokens.sync({
  logging: false,
});

module.exports = user_tokens;
