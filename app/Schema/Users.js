var Encrypt = require("./../Config/Encrypt");
var encrypt = new Encrypt();

var users = database.define(
  "users",
  {
    first_name: {
      type: Sequelize.STRING,
      defaultValue: "",
    },
    last_name: {
      type: Sequelize.STRING,
      defaultValue: "",
    },
    email: {
      type: Sequelize.STRING,
      set(val) {
        this.setDataValue("email", val.toLowerCase());
      },
    },
    password: {
      type: Sequelize.STRING,
      defaultValue: "",
    },
    country_code: {
      type: Sequelize.STRING(10),
      defaultValue: "",
    },
    phone: {
      type: Sequelize.STRING(20),
      defaultValue: "",
    },
    status: {
      type: Sequelize.BOOLEAN,
      defaultValue: 1,
    },
  },
  {
    underscored: true,
    hooks: {
      beforeCreate: (user, option) => {
        user.password = encrypt.encryptEntity(user.password);
        // user.auth_token = tokenGenerator.generator();
      },
    },
  }
);

users.sync({
  logging: false,
});

module.exports = users;
