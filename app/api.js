import axios from "axios";
import { notification } from "antd";
import jwt from "jsonwebtoken";
import { v1 as uuidv1 } from "uuid";
import { privateSecretKey } from "../utilities/constants";
import { BASE_URL } from "../utilities/constants";

const METHOD = {
  GET: "get",
  POST: "post",
  PUT: "put",
  DELETE: "delete",
};

// CHECK BELOW FOR SAMPLE DATA TO BE PASSED
class API {
  isLoggedIn = false;
  userToken = null;
  token = null;
  isGuest = false;

  constructor() {
    this.baseURL = BASE_URL;
  }

  generateDeviceToken() {
    let OSName = "Unknown OS";
    if (navigator.appVersion.indexOf("Win") !== -1) OSName = "Windows";
    if (navigator.appVersion.indexOf("Mac") !== -1) OSName = "MacOS";
    if (navigator.appVersion.indexOf("X11") !== -1) OSName = "UNIX";
    if (navigator.appVersion.indexOf("Linux") !== -1) OSName = "Linux";

    const uuid = uuidv1();
    const token = jwt.sign(
      {
        appversion: "1.0.0",
        appversioncode: "1",
        aud: ["com.xyz.abc"],
        exp: Math.floor(Date.now() / 1000) + 3600 * 3600,
        iat: Date.now(),
        id: uuidv1(),
        iss: "com.xyz.abc",
        manufacturer: "Xiaomi",
        model: "Redmi Note 5",
        notificationid: uuidv1(),
        os: "Android",
        osversion: "8.1.0",
        platform: "Mobile",
      },
      /*{
                aud: ['com.xyz.abc'],
                appversion: '1.0.0',
                appversioncode: '1',
                iat: Date.now(),
                id: uuidv1(),
                iss: 'com.xyz.abc',
                manufacturer: window.navigator.vendor,
                model: window.navigator.vendor,
                notificationid: uuidv1(),
                os: OSName,
                osversion: window.navigator.appVersion,
                platform: 'Web',
                exp: Math.floor(Date.now() / 1000) + 3600 * 3600,
            },*/
      privateSecretKey,
      { algorithm: "HS256" }
    );

    // Temporary Token for testing
    /*const token =
            'dadaeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJjb20uYXJjaGlzeXMuZ3JhYnBpbiIsImF1ZCI6WyJjb20uYXJjaGlzeXMuYXJ0aXMiXSwiYXBwdmVyc2lvbiI6MC4xLCJpZCI6IjQzYWY5OTkwLTk5MjYtMTFlYS05MWVhLTk1Y2FmNzY5ZDY3NSIsImlhdCI6MTU4OTE3Mzc5MSwibWFudWZhY3R1cmVyIjoiIiwibW9kZWwiOiIiLCJub3RpZmljYXRpb25pZCI6IiIsImFwcHZlcnNpb25jb2RlIjoiMzMiLCJvcyI6IkFuZHJvaWQiLCJvc3ZlcnNpb24iOiI4LjEuMCIsInBsYXRmb3JtIjoiTW9iaWxlIiwiZXhwIjoxNjAyNzgwMDIxfQ.UVRhMX89epgDjRdF6YtplkO-dVy6ddyUoqxfJsvq6ro'; */
    localStorage.setItem("deviceToken", token);

    // Method for decoding and verify token
    // const decodedToken = jwt.verify(token , privateSecretKey);
    // console.log("decodedToken" , decodedToken)
    return token;
  }

  getAuthenticationInfo() {
    if (
      localStorage.getItem("isLoggedIn") &&
      localStorage.getItem("authToken")
    ) {
      this.isLoggedIn = true;
      this.userToken = localStorage.getItem("userToken");
    } else if (localStorage.getItem("authToken")) {
      this.userToken = localStorage.getItem("authToken");
    }
  }

  // URL FOR API
  // REFER SAMPLE JSON AT BOTTOM FOR DATA VALUES
  get(url, data) {
    return new Promise((resolve, reject) => {
      this.api(METHOD.GET, url, data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }

  post(url, data) {
    return new Promise((resolve, reject) => {
      this.api(METHOD.POST, url, data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }

  put(url, data) {
    return new Promise((resolve, reject) => {
      this.api(METHOD.PUT, url, data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }

  delete(url, data) {
    return new Promise((resolve, reject) => {
      this.api(METHOD.DELETE, url, data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }

  api(method, url, data) {
    return new Promise((resolve, reject) => {
      // GET THE AUTH TOKEN
      this.getAuthenticationInfo();

      let axiosConfig = {};
      axiosConfig.method = method;
      axiosConfig.url = this.baseURL + url;

      axiosConfig.headers = this.setHeaders(data);
      if (data) {
        if (data.params) axiosConfig.params = data.params;
        if (data.data) axiosConfig.data = data.data;
      }

      axios(axiosConfig)
        .then((response) => {
          console.log("API -> api -> response", response);
          if (
            (response && response.StatusCode >= 400) ||
            response.StatusCode <= 500
          ) {
            notification.error({
              message:
                response.ResponseException &&
                response.ResponseException.ExceptionMessage,
            });
          } else {
            resolve(response.data);
          }
        })
        .catch((error) => {
          console.log("ERROR", error);
          notification.error({
            message: "Something went wrong! Please contact support.",
          });
          //DEFAULT ERROR HANDLING

          //localStorage.clear();
          //window.location = '/';
        })
        .finally((response) => {
          console.log("finally => ", response); // Could be success or error
        });
    });
  }

  setHeaders(data) {
    let headers = {};
    headers["accept-language"] = "en";
    headers["Content-Type"] = "application/json";

    if (localStorage.getItem("deviceToken")) {
      headers["Device"] = localStorage.getItem("deviceToken");
    } else {
      headers["Device"] = this.generateDeviceToken();
    }

    if (data) {
      if (data.isMultipart) {
        headers["Content-Type"] = "multipart/form-data";
      }

      if (data.headers) {
        for (var key in data.headers) {
          if (data.headers.hasOwnProperty(key)) {
            headers[key] = data.headers[key];
          }
        }
      }
    }

    if (!(data && data.skipAuth) && this.userToken) {
      headers["Authorization"] = `Bearer ${this.userToken}`;
    }

    return headers;
  }
}

export default API;
