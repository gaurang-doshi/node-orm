var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var PORT = process.env.PORT || 8000;

require('./app/Config/Global');
let Constants = require('./app/Config/Constant')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));
//app.use(bodyParser.text());

app.use(express.static(__dirname + '/public'));

//ROUTING
let usersRouter = require('./app/Routes/Users');
app.use('/users', usersRouter);

app.get('/', function (req, res) {
    res.send('NODE ORM DEMO BY.')
})

app.listen(PORT, function(){
    console.log('Listing on 8000');
})

// https://www.getpostman.com/collections/97c4c2f2b9fa71da374a